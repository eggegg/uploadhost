# == Schema Information
# Schema version: 8
#
# Table name: discussions
#
#  id         :integer         not null, primary key
#  name       :string(255)
#  user_id    :integer
#  title      :string(255)
#  body       :text
#  created_at :datetime
#  updated_at :datetime
#

class Discussion < ActiveRecord::Base
  has_many :replies
  belongs_to :user
  validates_presence_of :title
  validates_presence_of :body
end
