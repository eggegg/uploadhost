# == Schema Information
# Schema version: 8
#
# Table name: entries
#
#  id             :integer         not null, primary key
#  name           :string(255)
#  content_type   :string(255)
#  user_id        :integer
#  size           :integer
#  data           :binary(52428800
#  category_id    :integer
#  download_times :integer
#  description    :text
#  created_at     :datetime
#  updated_at     :datetime
#

class Entry < ActiveRecord::Base
  belongs_to :user
  belongs_to :category
  has_many :comments
  acts_as_taggable_on :tags
  named_scope :by_join_date, :order => "created_at DESC"
  
  def self.file_store?
    true  # false for database store
  end
  
  def before_destroy
    FileUtils.rm_rf(self.file_store_name) if Entry.file_store?
  end
  
  def validate_on_create
    errors.add(:file_field, "hasn't been uploaded.") unless self.size
  end
  
  def before_save
    self.name = base_part_of(self.name)
  end
  
  def after_create
    File.open(self.file_store_name,"w") {|f| f.write(@tosave.read)} if Entry.file_store?
  end
  
  def uploaded_file=(file_field)
    unless file_field.instance_of? String
      self.name = base_part_of(file_field.original_filename)
      self.content_type = file_field.content_type.chomp
      self.size = file_field.size
      unless Entry.file_store?
        self.data = file_field.read
      else
        @tosave = file_field
      end
    end
  end
  
  def file_store_name
    File.dirname(__FILE__)+"/../../uploads/"+(self.id.to_s)
  end
  
  def base_part_of(file_name)
    File.basename(file_name).gsub(/[^\w._-]/,'')
  end
  
  def self.per_page
    10
  end
  
  def self.find_category(id)
    all_entries = Array.new
    bfs = Array.new
    bfs << Category.find(id)
    while bfs.size > 0
      bfs += bfs[0].children 
      all_entries += bfs[0].entries
      bfs.delete_at(0)
    end
    all_entries.sort {|x,y| -1 * (x.created_at <=> y.created_at) }
  end
  
  def icon
    case self.content_type
      when /bmp/
        return "bmp.png"
      when /gif/
        return "gif.png"
      when /png/
        return "png.png"
      when /tif/
        return "tiff.png"
      when /word/
        return "word.png"
      when /powerpoint|presentation/
        return "powerpoint.png"
      when /excel/
        return "excel.png"
      when /psd/
        return "psd.png"
      when /pdf/
        return "pdf.png"
      else
        return "file.png"
    end
  end
end
