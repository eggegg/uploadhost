# == Schema Information
# Schema version: 8
#
# Table name: comments
#
#  id         :integer         not null, primary key
#  user_id    :integer
#  entry_id   :integer
#  body       :text
#  created_at :datetime
#  updated_at :datetime
#

class Comment < ActiveRecord::Base
  belongs_to :entry
  belongs_to :user
  validates_presence_of :body
end
