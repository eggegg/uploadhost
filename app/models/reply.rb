# == Schema Information
# Schema version: 8
#
# Table name: replies
#
#  id            :integer         not null, primary key
#  user_id       :integer
#  discussion_id :integer
#  body          :text
#  created_at    :datetime
#  updated_at    :datetime
#

class Reply < ActiveRecord::Base
  belongs_to :user
  belongs_to :discussion
  validates_presence_of :body
end
