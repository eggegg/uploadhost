# == Schema Information
# Schema version: 8
#
# Table name: categories
#
#  id        :integer         not null, primary key
#  name      :string(255)
#  parent_id :integer
#

class Category < ActiveRecord::Base
  has_many :entries
  acts_as_tree
  validates_presence_of :name

  def ancestors_name
    if parent
      parent.ancestors_name + parent.name + '::'
    else
      ""
    end
  end

  def long_name
    ancestors_name + name
  end
end
