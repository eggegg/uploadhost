class UploadController < ApplicationController
  before_filter :login_required 

  def get
    @entry = Entry.new
    @all_categories = Category.find(:all, :order => 'name')
  end

  def save
    @entry = Entry.new(params[:entry])
    @entry.user = current_user
    @entry.download_times = 0
    @entry.categories = Category.find(params[:categories]) if params[:categories]
    if @entry.save
      flash[:notice] = 'File was successfully uploaded.'
      redirect_to :action => 'show', :id => @entry.id
    else
      @all_categories = Category.find(:all, :order => 'name')
      render(:action => :get)
    end
  end

  def download
    @entry = Entry.find(params[:id])
    @entry.update_attribute(:download_times, @entry.download_times+1)
    if request.user_agent.downcase.include? "msie"
      @filename = CGI::escape(@entry.name)
    else
      @filename = @entry.name
    end
    if Entry.file_store?
      send_file @entry.file_store_name, :filename => @filename, :type => @entry.content_type
    else
      send_data @entry.data, :filename => @filename, :type => @entry.content_type
    end
  end

  def destroy
    current_user.entries.find(params[:id]).destroy
    redirect_to :controller => 'list', :action => 'show'
  end

  def edit
    @entry = current_user.entries.find(params[:id])
    @all_categories = Category.find(:all)
    if request.post?
      @entry.update_attributes(params[:entry])
      redirect_to :action => 'show', :id => params[:id]
    end
  end
  
  def show
    @entry = Entry.find(params[:id])
  end
  
  def comment
    return unless request.post?
    @entry = Entry.find(params[:id])
    @comment = @entry.comments.build(params[:comment])
    @comment.user = current_user
    @comment.save!
    flash[:notice] = "Successfully commented..."
    redirect_to :action => "show", :id => params[:id]
  rescue ActiveRecord::RecordInvalid
    render :action => "show", :id => params[:id]
  end
end
