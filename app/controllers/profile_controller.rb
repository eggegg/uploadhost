class ProfileController < ApplicationController
  before_filter :login_required 
  layout "upload"
  
  def edit
    @user = current_user
    return unless request.post?
    if @user.update_attributes(params[:user])
      flash[:notice] = 'Your profile was successfully updated.'
      redirect_to :action => 'edit'
    else
      render :action => 'edit'
    end
  end
end
