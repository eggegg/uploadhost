class CategoriesController < ApplicationController
  before_filter :login_required
  layout "upload"
  
  def index
    list
    render :action => 'list'
  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }

  def list
    @all_categories = Category.find(:all, :order => 'name')
  end

  def new
    @category = Category.new
    @all_categories = Category.find(:all, :order => 'name')
  end

  def create
    @category = Category.new(params[:category])
    if @category.save
      flash[:notice] = 'Category was successfully created.'
      redirect_to :action => 'list'
    else
      render :action => 'new'
    end
  end

  def edit
    redirect_to :action => 'list' if params[:id] == '1'
    @category = Category.find(params[:id])
    #@all_categories = Category.find(:all, :order => 'name')
    @all_categories = Array.new
    for c in Category.find(:all)
      @all_categories << c if !c.ancestors.include?(@category) && c != @category
    end
  end

  def update
    redirect_to :action => 'edit' if params[:id] == params[:category][:parent_id]
    @category = Category.find(params[:id])
    @all_categories = Array.new
    for c in Category.find(:all)
      @all_categories << c if !c.ancestors.include?(@category) && c != @category
    end
    if @category.update_attributes(params[:category])
      flash[:notice] = 'Category was successfully updated.'
      redirect_to :action => 'list'
    else
      render :action => 'edit'
    end
  end
end
