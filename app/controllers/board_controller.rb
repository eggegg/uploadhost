class BoardController < ApplicationController
  before_filter :login_required
  layout 'upload'
  
  def list
    @discussions = Discussion.paginate :page => params[:page], :order => "created_at DESC"
  end
  
  def show
    @discussion = Discussion.find(params[:id])
  end
  
  def new
    return unless request.post?
    @discussion = Discussion.new(params[:discussion])
    @discussion.user = current_user
    if @discussion.save
      flash[:notice] = "Successfully created..."
      redirect_to :action => "list"
    else
      flash[:notice] = "Something got wrong..."
      render :action => "new"
    end
  end
  
  def reply
    return unless request.post?
    @discussion = Discussion.find(params[:id])
    @reply = @discussion.replies.build(params[:reply])
    @reply.user = current_user
    if @reply.save
      flash[:notice] = "Successfully created..."
      redirect_to :action => "show", :id => params[:id]
    else
      flash[:notice] = "Something got wrong..."
      render :action => "show", :id => params[:id]
    end
  end
end
