class ListController < ApplicationController
  before_filter :login_required
  layout "upload"

  def show
    @search = 'filename'
    if params[:keyword]
      @entries = Entry.paginate :conditions => ["name LIKE ?","%#{params[:keyword]}%"], :page => params[:page], :order => "created_at DESC", :per_page => 10
    else
      @entries = Entry.paginate :page => params[:page], :order => "created_at DESC"
    end
    render :action => 'show'
  end
  
  def uploader
    @search = 'uploader'
    if params[:keyword]
      c = User.find_by_login(params[:keyword])
      if c != nil
        params[:id] = c.id
      else
        flag = true
      end
    end
    unless params[:id] || params[:keyword]
      @newsearch = true
      @entries = [].paginate
      render :action => 'show'
      return
    end    
    @entries = (flag) ? [].paginate : (Entry.paginate_by_user_id params[:id], :page => params[:page], :order => "created_at DESC")
    @toshow = "User : #{User.find(params[:id]).login}" unless flag 
    render :action => 'show'
  end
  
  def category
    @search = 'category'
    if params[:keyword]
      c = Category.find_by_name(params[:keyword])
      if c != nil
        params[:id] = c.id
      else
        flag = true
      end
    end
    unless params[:id] || params[:keyword]
      @newsearch = true
      @entries = [].paginate
      render :action => 'show'
      return
    end
    @entries = (flag) ? [].paginate : (Entry.find_category(params[:id]).paginate :page => params[:page], :per_page => 10)
    @toshow = "Category : #{Category.find(params[:id]).long_name}" unless flag   
    render :action => 'show'  end
  
  def tag
    @search = "tag"
    params[:id] = params[:keyword] if params[:keyword]
    unless params[:id] || params[:keyword]
      @newsearch = true
      @entries = [].paginate
      render :action => 'show'
      return
    end
    @entries = Entry.find_tagged_with(params[:id],:on => :tags).sort {|x,y| -1*(x.created_at <=> y.created_at)}.paginate :page => params[:page], :per_page => 10
    @toshow = "Tag : #{params[:id]}"    
    render :action => 'show'
  end
  
  alias_method :filename, :show
end
