class CreateEntries < ActiveRecord::Migration
  def self.up
    create_table :entries do |t|
      t.column :name, :string
      t.column :content_type, :string
      t.column :user_id, :integer
      t.column :size, :integer
      t.column :data, :binary, :limit => 50.megabyte
      t.column :category_id, :integer
      t.column :download_times, :integer
      t.column :description, :text
      t.timestamps
    end
  end

  def self.down
    drop_table :entries
  end
end
